import { arrTheme } from "../../Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme"
import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK, UPDATE_TASK } from "../types/ToDoListTypes"

const initialState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
        { id: 'task-1', taskName: 'task 1', done: true },
        { id: 'task-2', taskName: 'task 2', done: false },
        { id: 'task-3', taskName: 'task 3', done: true },
        { id: 'task-4', taskName: 'task 4', done: false },
    ],
    taskEdit:
        { id: '-1', taskName: '', done: false },



}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK: {
            //kiểm tra rỗng
            if (action.newTask.name === '') {
                alert("Task name is required!");
                return { ...state }
            }

            // kiểm tra tồn tại
            let taskListupdate = [...state.taskList]
            let index = taskListupdate.findIndex(task => task.taskName === action.newTask.taskName)
            if (index !== -1) {
                alert("task name already exists!")
                return { ...state }
            }
            taskListupdate.push(action.newTask);
            //xử lý xong thì ggasn tákList mới vào taskList
            state.taskList = taskListupdate

            return { ...state }
        }
        case CHANGE_THEME: {
            // tìm theme dựa vào action,themID đƯợc chọn
            let theme = arrTheme.find(theme => theme.id == action.themeID);
            if (theme) {
                //set lại theme cho state.themeToDoList
                state.themeToDoList = { ...theme.theme };
            }
            return { ...state }
        }
        case DONE_TASK: {
            //click vào button check => dispatch lên action có tákID
            let taskListUpdate = [...state.taskList];
            //tìm ták id tìm ra ták đó ở vị trí nào trong mảng tiến hàn cập nhật lại thuộc tính done = true 
            //.và cập nhập lại state của redux
            let index = taskListUpdate.findIndex(task => task.id === action.taskId);
            if (index !== -1) {
                taskListUpdate[index].done = true;
            }
            // state.taskList = taskListUpdate;
            return { ...state, taskList: taskListUpdate }
        }

        case DELETE_TASK: {
            let taskListUpdate = [...state.taskList];
            //gán lại giá trị cho mảng tákListUpdate = chính nó nhưng filter không có taskId đó
            taskListUpdate = taskListUpdate.filter(task => task.id !== action.taskId)
            return { ...state, taskList: taskListUpdate }
        }
        case EDIT_TASK: {
            return { ...state, taskEdit: action.task }
        }
        case UPDATE_TASK: {
            state.taskEdit = { ...state.taskEdit, taskName: action.taskName }

            let taskListUpdate = [...state.taskList]

            let index = taskListUpdate.findIndex(task => task.id === state.taskEdit.id)

            if (index !== -1) {
                taskListUpdate[index] = state.taskEdit
            }
            state.taskList = taskListUpdate
            state.taskEdit = { id: '-1', taskName: '', done: false }

            return { ...state }
        }
        default:
            return { ...state }
    }
}
