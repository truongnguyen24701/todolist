import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK, UPDATE_TASK } from "../types/ToDoListTypes";




export const addTaskAction = (newTask) => {
    return {
        type: ADD_TASK,
        newTask
    }
}


export const changeThemeAction = (themeID) => {
    return {
        type: CHANGE_THEME,
        themeID
    }
}


export const doneTaskAction = (taskId) => ({
    type: DONE_TASK,
    taskId
})


export const deleteTaskAction = (taskId) => ({
    type: DELETE_TASK,
    taskId
})

export const editTaskAction = (task) => ({
    type: EDIT_TASK,
    task
})

export const updateTask = (taskName) => ({ 
    type:UPDATE_TASK,
    taskName
 })

//{ return} = ({})